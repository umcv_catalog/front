/* eslint-disable @typescript-eslint/no-var-requires */
const path = require('path');

module.exports = {
  // eslint-disable-next-line global-require
  webpackConfig: require('./webpack/styleguide.webpack.js'),
  // eslint-disable-next-line global-require
  propsParser: require('react-docgen-typescript').parse,
  styleguideComponents: {
    Wrapper: path.join(__dirname, 'src/helpers/StyleguideProvider.tsx')
  },
  sections: [
    {
      name: 'Pages',
      content: 'src/pages/pages.md',
      components: ['src/pages/**/*.tsx']
    },
    {
      name: 'Components',
      content: 'src/components/components.md',
      components: ['src/components/**/*.tsx']
    }
  ]
};
