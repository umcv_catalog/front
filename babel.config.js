module.exports = {
  presets: [
    '@babel/preset-env',
    '@babel/preset-react',
    '@babel/preset-typescript'
  ],
  plugins: [
    ['babel-plugin-styled-components',
      {
        ssr: false,
        preprocess: false,
        displayName: false
      }],
    ['@babel/transform-runtime', {
      regenerator: true
    }],
    ['@babel/plugin-transform-modules-commonjs']
  ]
};
