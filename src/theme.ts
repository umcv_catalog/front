import { DefaultTheme } from 'styled-components';

class Theme implements DefaultTheme {

  public animationDuration = '.2s';
  public borderRadius = '4px';

  public color = {
    Background: {
      BluredBg: 'rgba(2, 6, 15, 0.75)',
      Subheader: '#526599',
      WebPage: '#fcfcfc',
    },
    Divider: {
      Dark: '#dbdbdb',
      Light: '#e6e6e6',
    },
    Main: {
      AccentAdditional: '#16b29f',
      AccentMain: '#ff3440',
      Bill: '#2f3a58',
      Chips: '#606f99',
      InactiveSelection: '#d0dae2',
      Neutral: '#f2f2f2',
    },
    Text: {
      MainTextDark: '#25273e',
      TitleDark: '#1a1e47',
      White: '#9D9D9D',
    },
  };

  public fontSize = {
    xs: '10px',
    sm: '12px',
    md: '14px',
    lg: '18px',
    xl: '24px',
  };

  public intentColor = {
    danger: '#ff3440',
    default: '#606f99',
    info: '#606f99',
    none: '#d0dae2',
    primary: '#ff3440',
    success: '#16b29f',
    warning: '#ff3440',
  };

  public lineHeight = '1.4em';
  public spacing = {
    xs: '4px',
    sm: '8px',
    md: '16px',
    lg: '24px',
    xl: '32px',
  };

  public screenSize = {
    xs: '320px',
    sm: '544px',
    md: '768px',
    lg: '992px',
    xl: '1174px'
  };

}

export {
  Theme
};
