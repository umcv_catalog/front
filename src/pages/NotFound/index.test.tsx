import { shallow } from 'enzyme';
import React from 'react';

import NotFoundPage from '.';

describe('Not found page testing', () => {
  it('Render not found page', () => {
    const component = shallow(<NotFoundPage />);
    expect(component.exists('ForwardRef(Typography)')).toBeTruthy();
  });
});
