import Link from '@mui/material/Link';
import Typography from '@mui/material/Typography';
import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import config from 'Root/config';

import * as Style from './style';

const NotFoundPage: React.FC = () => (
  <Style.Container>
    <Style.Image
      src="https://www.vectorlogo.zone/logos/reactjs/reactjs-icon.svg"
      alt=""
    />
    <Typography align="center" variant="h4">
      Page not found
    </Typography>
    <Link component={RouterLink} to={config.routePath.ROOT}>To start page</Link>
  </Style.Container>
);

NotFoundPage.displayName = 'NotFoundPage';

export default NotFoundPage;
