import Alert from '@mui/material/Alert';
import CatalogControls from 'Components/CatalogControls';
import CatalogList from 'Components/CatalogList';
import SiteLoader from 'Components/common/SiteLoader';
import withAuth from 'Hoc/withAuth';
import withStore from 'Hoc/withStore';
import { observer } from 'mobx-react-lite';
import React, { useEffect, useState } from 'react';
import CatalogService from 'Services/catalog';
import AbstractCatalogStore from 'Specs/stores/catalogStore';
import catalogStore from 'Stores/catalogStore';

import * as Style from './style';

interface ICatalogPageProps {
  catalog: AbstractCatalogStore;
}

const CatalogPage: React.FC<ICatalogPageProps> = observer(({ catalog }) => {
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    const catalogService = new CatalogService();
    catalogService.getListChunkInfo()
      .finally(() => {
        setLoading(false);
      });
  }, []);

  if (loading) {
    return (
      <SiteLoader />
    );
  }

  return (
    <Style.Container>
      <CatalogControls catalog={catalog} />
      <CatalogList catalog={catalog} />
      {catalog.errorMessage && (
        <Alert severity="error">
          {catalog.errorMessage}
        </Alert>
      )}
    </Style.Container>
  );
});

export default withStore(withAuth(CatalogPage), { catalog: catalogStore });
