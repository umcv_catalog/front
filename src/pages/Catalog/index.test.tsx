import 'jest-styled-components';

import { shallow } from 'enzyme';
import { mountWithTheme } from 'Hoc/withTheme';
import React from 'react';
import { Theme } from 'Root/theme';
import { LOGIN_STATUS } from 'Specs/login-status';
import authStore from 'Stores/authStore';
import catalogStore from 'Stores/catalogStore';

import Catalog from './index';
import { Container } from './style';

jest.mock('Services/auth');
// eslint-disable-next-line func-names
jest.mock('Services/catalog', () => function () {
  return { getListChunkInfo: () => new Promise<void>(resolve => resolve()) };
});

describe('Catalog page testing', () => {

  it('Render Catalog page', () => {
    authStore.setStatus(LOGIN_STATUS.LOGGEDIN);
    const component = shallow(<Catalog.WrappedComponent catalog={catalogStore} />);
    expect(component.exists('SiteLoader')).toBeTruthy();
  });

  it('Render Styled elements', () => {
    const theme = new Theme();
    const styledContainer = mountWithTheme(<Container />);
    expect(styledContainer).toHaveStyleRule('margin-top', theme.spacing.md);
  });
});
