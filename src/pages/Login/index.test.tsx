import 'jest-styled-components';

import { shallow } from 'enzyme';
import { mountWithTheme } from 'Hoc/withTheme';
import React from 'react';
import { Theme } from 'Root/theme';
import { LOGIN_STATUS } from 'Specs/login-status';
import authStore from 'Stores/authStore';

import Login from './index';
import { Container, Loader } from './style';

jest.mock('react-router', () => ({
  useHistory: () => ({ replace: jest.fn() }),
}));

describe('Login page testing', () => {
  it('Render Login page', () => {
    const component = shallow(<Login.WrappedComponent auth={authStore} />);
    expect(component.exists('Loader')).toBeTruthy();
  });

  it('Render Login page as authorized', () => {
    authStore.setStatus(LOGIN_STATUS.LOGGEDIN);
    const component = shallow(<Login.WrappedComponent auth={authStore} />);
    expect(component.exists('LoginForm')).toBeFalsy();
  });

  it('Render Login page as unauthorized', () => {
    authStore.setStatus(LOGIN_STATUS.NOTLOGGEDIN);
    const component = shallow(<Login.WrappedComponent auth={authStore} />);
    expect(component.exists('LoginForm')).toBeTruthy();
  });

  it('Render Styled elements', () => {
    const theme = new Theme();
    const styledContainer = mountWithTheme(<Container />);
    expect(styledContainer).toHaveStyleRule('margin-top', theme.spacing.md);
    const styledLoader = mountWithTheme(<Loader />);
    expect(styledLoader).toHaveStyleRule('margin', `${theme.spacing.md} auto`);
  });
});
