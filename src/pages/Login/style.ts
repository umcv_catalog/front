import CircularProgress from '@mui/material/CircularProgress';
import styled from 'styled-components';

export const Container = styled.div`
  margin-top: ${({ theme }) => theme.spacing.md};
`;

export const Loader = styled(CircularProgress)`
  margin: ${({ theme }) => theme.spacing.md} auto;
`;
Loader.displayName = 'Loader';
