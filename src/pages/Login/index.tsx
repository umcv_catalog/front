import LoginForm from 'Components/common/Login';
import withStore from 'Hoc/withStore';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { useHistory } from 'react-router';
import config from 'Root/config';
import { LOGIN_STATUS } from 'Specs/login-status';
import AbstractAuthStore from 'Specs/stores/authStore';
import authStore from 'Stores/authStore';

import * as Style from './style';

interface ILoginPageProps {
  auth: AbstractAuthStore;
}

const LoginPage: React.FC<ILoginPageProps> = observer(({ auth }) => {
  const history = useHistory();
  useEffect(() => {
    if (auth.isAuthorized) {
      history.replace(config.routePath.ROOT);
    }
  }, [auth.isAuthorized]);

  if (auth.isAuthorized) {
    return null;
  }

  if (auth.status !== LOGIN_STATUS.NOTLOGGEDIN) {
    return (
      <Style.Loader />
    );
  }

  return (
    <Style.Container>
      <LoginForm />
    </Style.Container>
  );
});

export default withStore(LoginPage, { auth: authStore });
