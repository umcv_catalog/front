import { shallow } from 'enzyme';
import React from 'react';
import { SummaryInfoService } from 'Services/summaryInfo';
import { LOGIN_STATUS } from 'Specs/login-status';
import authStore from 'Stores/authStore';
import summaryInfoStore from 'Stores/summaryInfoStore';

import StartPage from '.';

jest.mock('Services/summaryInfo');

describe('Start page testing', () => {
  it('Render Start page', () => {
    const component = shallow(<div><StartPage.WrappedComponent auth={authStore} summaryInfo={summaryInfoStore} /></div>);
    const container = component.find('StartPage').dive();
    expect(container.exists('SummaryInfo')).toBeTruthy();
  });

  it('Render Start page logged in', () => {
    authStore.setStatus(LOGIN_STATUS.LOGGEDIN);
    const component = shallow(<div><StartPage.WrappedComponent auth={authStore} summaryInfo={summaryInfoStore} /></div>);
    component.find('StartPage').dive();
    expect(SummaryInfoService).toHaveBeenCalled();
  });
});
