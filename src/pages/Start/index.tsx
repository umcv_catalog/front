import SummaryInfo from 'Components/SummaryInfo';
import withStore from 'Hoc/withStore';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { SummaryInfoService } from 'Services/summaryInfo';
import AbstractAuthStore from 'Specs/stores/authStore';
import AbstractSummaryInfoStore from 'Specs/stores/summaryInfo';
import authStore from 'Stores/authStore';
import summaryInfoStore from 'Stores/summaryInfoStore';

interface ISummaryInfoProps {
  auth: AbstractAuthStore;
  summaryInfo: AbstractSummaryInfoStore;
}

const StartPage: React.FC<ISummaryInfoProps> = observer(({ auth, summaryInfo }) => {

  useEffect(() => {
    if (auth.isAuthorized) {
      const summaryInfoService = new SummaryInfoService();
      summaryInfoService.getSummaryInfo();
    }
  }, [auth.isAuthorized]);

  return (
    <SummaryInfo auth={auth} summaryInfo={summaryInfo} />
  );
});

StartPage.displayName = 'StartPage';

export default withStore(StartPage, { auth: authStore, summaryInfo: summaryInfoStore });
