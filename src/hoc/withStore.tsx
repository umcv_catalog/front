import React from 'react';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
function withStore<P, S>(
  WrappedComponent: React.ComponentType<P> & { WrappedComponent?: React.ComponentType<any>},
  storeObject: S
) {
  const displayName = WrappedComponent.displayName || WrappedComponent.name || 'Component';
  const StoreProvider = (props: Omit<P, keyof S>): React.ReactElement => (
    <WrappedComponent
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props as P}
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...storeObject as S}
    />
  );
  StoreProvider.displayName = displayName;
  StoreProvider.WrappedComponent = WrappedComponent.WrappedComponent ?? WrappedComponent;

  return StoreProvider;
}

export default withStore;
