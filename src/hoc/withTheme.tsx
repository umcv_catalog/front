import { mount, ReactWrapper, shallow, ShallowWrapper } from 'enzyme';
import React from 'react';
import { Theme } from 'Root/theme';
import { ThemeProvider } from 'styled-components';

export const shallowWithTheme = (children: React.ReactElement): ShallowWrapper => shallow(
  <ThemeProvider theme={new Theme()}>
    { children }
  </ThemeProvider>
);

export const mountWithTheme = (children: React.ReactElement): ReactWrapper => mount(
  <ThemeProvider theme={new Theme()}>
    { children }
  </ThemeProvider>
);
