import SiteLoader from 'Components/common/SiteLoader';
import withStore from 'Hoc/withStore';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { useHistory } from 'react-router';
import config from 'Root/config';
import { LOGIN_STATUS } from 'Specs/login-status';
import AbstractAuthStore from 'Specs/stores/authStore';
import authStore from 'Stores/authStore';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
function withAuth<P>(
  WrappedComponent: React.ComponentType<P> & { WrappedComponent?: React.ComponentType<P>}
) {
  const displayName = WrappedComponent.displayName || WrappedComponent.name || 'Component';

  interface IAuthCheck {
    auth: AbstractAuthStore;
  }
  const AuthCheck = (props: P & IAuthCheck): React.ReactElement => {
    const { auth } = props;
    const history = useHistory();
    if (auth.status === LOGIN_STATUS.NOTLOGGEDIN) {
      history.push(config.routePath.ROOT);
      return (
        <SiteLoader />
      );
    }

    if (auth.status === LOGIN_STATUS.LOGGEDIN) {
      return (
        <WrappedComponent {...props as P} />
      );
    }

    return (
      <SiteLoader />
    );
  };
  AuthCheck.displayName = displayName;
  AuthCheck.WrappedComponent = WrappedComponent.WrappedComponent ?? WrappedComponent;

  return withStore(observer(AuthCheck), { auth: authStore });
}

export default withAuth;
