import React from 'react';
import { Theme } from 'Root/theme';
import { ThemeProvider } from 'styled-components';

export default function Provider({ children }: { children: React.ReactNode}): React.ReactNode {
  return <ThemeProvider theme={new Theme()}>{children}</ThemeProvider>;
}
