import React from 'react';

interface Html {
  scripts: string[];
  stylesheets: string[];
  title: string;
  styles: string;
}

export const Html = ({ children, scripts, stylesheets, title }: React.PropsWithChildren<Html>): React.ReactElement => (
  <html lang="en">
    <head>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width,minimum-scale=1,maximum-scale=1,initial-scale=1" />
      <meta httpEquiv="X-UA-Compatible" content="ie=edge" />
      <title>{title}</title>
      {stylesheets.map(stylesheet => <link rel="stylesheet" type="text/css" href={stylesheet} key={stylesheet} />)}
    </head>
    <body>
      <div id="root">{children}</div>
      {scripts.map(script => <script src={script} key={script} />)}
    </body>
  </html>
);
