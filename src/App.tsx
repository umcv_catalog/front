import Footer from 'Components/common/Footer';
import Header from 'Components/common/Header';
import { configurePersistable } from 'mobx-persist-store';
import React from 'react';
import canUseDOM from 'Root/utils/canUseDom';
import Router from 'Router';
import { ThemeProvider } from 'styled-components';

import { GlobalStyle } from './styles/GlobalStyle';
import { Theme } from './theme';

configurePersistable(
  {
    storage: canUseDOM ? window.localStorage : undefined,
    stringify: false,
    debugMode: true,
  }
);

const App = (): React.ReactElement => (
  <ThemeProvider theme={new Theme()}>
    <GlobalStyle />
    <Header />
    <Router />
    <Footer />
  </ThemeProvider>
);

export default App;
