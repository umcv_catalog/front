import { shallow } from 'enzyme';
import React from 'react';

import App from './App';

describe('App container testing', () => {
  it('Render App', () => {
    const component = shallow(<App />);
    expect(component.exists('Routes')).toBeTruthy();
  });
});
