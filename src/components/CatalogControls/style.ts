import styled from 'styled-components';

export const Container = styled.div`
  margin: ${({ theme }) => theme.spacing.md};
  margin-top: 0;
`;
