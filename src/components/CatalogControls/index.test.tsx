import 'jest-styled-components';

import { shallow } from 'enzyme';
import { mountWithTheme } from 'Hoc/withTheme';
import React from 'react';
import { Theme } from 'Root/theme';
import { TAvailableColumns } from 'Specs/models';
import catalogStore from 'Stores/catalogStore';
import { exampleColumns } from 'Stores/catalogStore.test';

import CatalogControls from '.';
import { Container } from './style';

// eslint-disable-next-line func-names
jest.mock('Services/catalog', () => function () {
  return { getAvailableColumns: () => new Promise<TAvailableColumns | null>(resolve => resolve(exampleColumns)) };
});

describe('CatalogControls testing', () => {
  it('Render CatalogControls', () => {
    catalogStore.availableColumns = null;
    const component = shallow(<CatalogControls catalog={catalogStore} />);
    expect(component.exists('ForwardRef(Button)')).toBeTruthy();
  });

  it('Open and close columns dialog', () => {
    const component = shallow(<CatalogControls catalog={catalogStore} />);
    const button = component.find('ForwardRef(Button)');
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    button.prop('onClick')();

    expect(component.exists('ForwardRef(Dialog)')).toBeTruthy();
    const dialog = component.find('ForwardRef(Dialog)').first();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    dialog.prop('onClose')();
    catalogStore.setAvailableColumns(exampleColumns);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    button.prop('onClick')();
  });

  it('Render Styled elements', () => {
    const theme = new Theme();
    const styledContainer = mountWithTheme(<Container />);
    expect(styledContainer).toHaveStyleRule('margin', theme.spacing.md);
  });
});
