import BlurLinearIcon from '@mui/icons-material/BlurLinear';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import Dialog from '@mui/material/Dialog';
import ColumnsSelectorDialog from 'Components/ColumnsSelectorDialog';
import { observer } from 'mobx-react-lite';
import React, { useCallback, useState } from 'react';
import CatalogService from 'Services/catalog';
import AbstractCatalogStore from 'Specs/stores/catalogStore';

import * as Styled from './style';

interface ICatalogControlsProps {
  catalog: AbstractCatalogStore;
}

const CatalogControls: React.FC<ICatalogControlsProps> = observer(({ catalog }) => {
  const [columnsModalOpened, setColumnsModalOpened] = useState(false);
  const [columnsInfoLoading, setColumnsInfoOpened] = useState(false);
  const catalogService = new CatalogService();

  const handleOpenColumnsSettingsModal = useCallback(() => {
    if (catalog.availableColumns) {
      catalog.setCurrentColumns(catalog.availableColumns);
      setColumnsModalOpened(true);
    } else {
      setColumnsInfoOpened(true);
      catalogService.getAvailableColumns()
        .then(() => {
          if (catalog.availableColumns) {
            catalog.setCurrentColumns(catalog.availableColumns);
          }
          setColumnsModalOpened(true);
        })
        .finally(() => {
          setColumnsInfoOpened(false);
        });
    }
  }, []);

  const handleCloseColumnsSettingsModal = useCallback(() => {
    setColumnsModalOpened(false);
  }, []);

  return (
    <Styled.Container>
      <Button
        variant="outlined"
        startIcon={<BlurLinearIcon />}
        onClick={handleOpenColumnsSettingsModal}>
        {columnsInfoLoading ? <CircularProgress /> : 'Columns'}
      </Button>
      <Dialog
        open={columnsModalOpened}
        onClose={handleCloseColumnsSettingsModal}>
        <ColumnsSelectorDialog
          catalog={catalog}
          onClose={handleCloseColumnsSettingsModal}
        />
      </Dialog>
    </Styled.Container>
  );
});

CatalogControls.displayName = 'CatalogControls';

export default CatalogControls;
