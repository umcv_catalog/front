import 'jest-styled-components';

import { shallow } from 'enzyme';
import { mountWithTheme } from 'Hoc/withTheme';
import React from 'react';
import { Theme } from 'Root/theme';
import { AuthService } from 'Services/auth';
import { LOGIN_STATUS } from 'Specs/login-status';
import authStore from 'Stores/authStore';

import Header from '.';
import { Info, Loader, Nav, Username } from './style';

jest.mock('Services/auth');

describe('Header testing', () => {
  it('Render Header', () => {
    const component = shallow(<Header.WrappedComponent auth={authStore} />);
    expect(component.exists('Nav')).toBeTruthy();
    const navLink = component.find('NavLink[onClick]');
    navLink.simulate('click');
    const dialog = component.find('ForwardRef(Dialog)');
    dialog.simulate('close');
  });

  it('Render Header Loader', () => {
    authStore.setStatus(LOGIN_STATUS.PENDING);
    const component = shallow(<Header.WrappedComponent auth={authStore} />);
    expect(component.exists('Loader')).toBeTruthy();
  });

  it('Render Header logged in', () => {
    authStore.setStatus(LOGIN_STATUS.LOGGEDIN);
    const component = shallow(<Header.WrappedComponent auth={authStore} />);
    const navLink = component.find('NavLink[onClick]');
    navLink.simulate('click');
    expect(AuthService).toHaveBeenCalled();
  });

  it('Render Styled elements', () => {
    const theme = new Theme();
    const styledNav = mountWithTheme(<Nav />);
    expect(styledNav).toHaveStyleRule('background-color', theme.color.Main.Bill);
    const styledInfo = mountWithTheme(<Info />);
    expect(styledInfo).toHaveStyleRule('color', theme.color.Text.White);
    const styledLoader = mountWithTheme(<Loader />);
    expect(styledLoader).toHaveStyleRule('padding', theme.spacing.md);
    const styledUsername = mountWithTheme(<Username />);
    expect(styledUsername).toHaveStyleRule('padding', theme.spacing.md);
  });
});
