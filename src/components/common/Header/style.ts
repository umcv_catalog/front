import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import styled from 'styled-components';

export const NavLink = styled(Button)``;
NavLink.displayName = 'NavLink';

export const Nav = styled.nav`
  background-color: ${({ theme }) => theme.color.Main.Bill};
  border-bottom: 1px solid ${({ theme }) => theme.color.Main.AccentAdditional};
  position: fixed;
  top: 0;
  right: 0;
  left: 0;
  display: flex;
  justify-content: space-between;

  ${NavLink} {
    padding: ${({ theme }) => theme.spacing.md};
    line-height: 24px;
  }
`;
Nav.displayName = 'Nav';

export const Info = styled.div`
  display: flex;
  align-items: center;
  font-size: ${({ theme }) => theme.fontSize.lg};
  color: ${({ theme }) => theme.color.Text.White};
`;

export const Username = styled.span`
  padding: ${({ theme }) => theme.spacing.md};
  line-height: 24px;
`;

export const Navbar = styled.div`
  display: flex;
`;

export const Loader = styled(CircularProgress)`
  padding: ${({ theme }) => theme.spacing.md};
`;
Loader.displayName = 'Loader';
