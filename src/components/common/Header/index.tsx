import Dialog from '@mui/material/Dialog';
import Link from '@mui/material/Link';
import withStore from 'Hoc/withStore';
import { observer } from 'mobx-react-lite';
import React, { useEffect } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import config from 'Root/config';
import { AuthService } from 'Services/auth';
import { LOGIN_STATUS } from 'Specs/login-status';
import AbstractAuthStore from 'Specs/stores/authStore';
import authStore from 'Stores/authStore';

import LoginForm from '../Login';
import * as Style from './style';

interface IHeaderProps {
  auth: AbstractAuthStore;
}

const Header: React.FC<IHeaderProps> = observer(({ auth }): React.ReactElement => {

  const authService = new AuthService();

  useEffect(() => {
    authService.getMe();
  }, []);

  function handleLogin() {
    auth.setErrorMessage(null);
    auth.setLoginDialogOpened(true);
  }

  function handleLogout() {
    authService.logOut();
  }

  function handleDialogClose() {
    auth.setLoginDialogOpened(false);
  }

  function renderLoginButton() {
    if (auth.status === LOGIN_STATUS.PENDING) {
      return (
        <Style.Loader />
      );
    }
    return (
      <>
        {
          auth.isAuthorized
            ? (
              <Style.NavLink onClick={handleLogout}>
                Logout
              </Style.NavLink>
            )
            : (
              <Style.NavLink onClick={handleLogin}>
                Sign in
              </Style.NavLink>
            )
        }
      </>
    );
  }

  return (
    <Style.Nav role="navigation">
      <Style.Info>
        <Style.Username>
          <Link component={RouterLink} to={config.routePath.ROOT}>Start page</Link>
        </Style.Username>
        {auth.user !== null && (
          <Style.Username>
            User:
            {auth.user.username}
          </Style.Username>
        )}
      </Style.Info>
      <Style.Navbar>
        { renderLoginButton() }
      </Style.Navbar>
      <Dialog
        open={auth.isLoginDialogOpened}
        onClose={handleDialogClose}>
        <LoginForm />
      </Dialog>
    </Style.Nav>
  );
});

export default withStore(Header, { auth: authStore });
