import 'jest-styled-components';

import { shallow } from 'enzyme';
import { mountWithTheme } from 'Hoc/withTheme';
import React from 'react';
import { Theme } from 'Root/theme';
import { AuthService } from 'Services/auth';
import authStore from 'Stores/authStore';

import Login from '.';
import { Container, Separator } from './style';

jest.mock('Services/auth');

describe('Login form testing', () => {
  it('Render Login', () => {
    const component = shallow(<Login.WrappedComponent auth={authStore} />);
    expect(component.exists('ForwardRef(Button)')).toBeTruthy();
    component.find('ForwardRef(Button)').simulate('click');
    const loginField = component.find('ForwardRef(TextField)[label="Login"]');
    const passwordField = component.find('ForwardRef(TextField)[label="Password"]');
    loginField.simulate('change', { target: { value: 'test' } });
    passwordField.simulate('change', { target: { value: 'test' } });
    loginField.simulate('keyup', { key: 'a' });
    loginField.simulate('keyup', { key: 'Enter' });
    passwordField.simulate('keyup', { key: 'b' });
    passwordField.simulate('keyup', { key: 'Enter' });
    expect(AuthService).toHaveBeenCalled();
  });

  it('check focused field login on mount', () => {
    const componentEmpty = shallow(<div><Login.WrappedComponent auth={authStore} /></div>);
    componentEmpty.find('LoginForm').dive();
  });

  it('check focused field password on mount', () => {
    authStore.setLoginInfo(
      { login: { value: '123', isInvalid: false, isValid: true }, password: { value: '', isInvalid: false, isValid: false } }
    );
    const componentWithLogin = shallow(<div><Login.WrappedComponent auth={authStore} /></div>);
    componentWithLogin.find('LoginForm').dive();
  });

  it('check focused filled fields on mount', () => {
    authStore.setLoginInfo(
      { login: { value: '123', isInvalid: false, isValid: true }, password: { value: 'test', isInvalid: false, isValid: true } }
    );
    const componentWithPassword = shallow(<div><Login.WrappedComponent auth={authStore} /></div>);
    componentWithPassword.find('LoginForm').dive();
  });

  it('check login on enter', () => {
    authStore.setLoginInfo(
      { login: { value: '123', isInvalid: false, isValid: true }, password: { value: 'test', isInvalid: false, isValid: true } }
    );
    const component = shallow(<Login.WrappedComponent auth={authStore} />);
    const passwordField = component.find('ForwardRef(TextField)[label="Password"]');
    passwordField.simulate('keyup', { key: 'Enter' });
    expect(AuthService).toHaveBeenCalled();
  });

  it('Render Styled elements', () => {
    const theme = new Theme();
    const styledContainer = mountWithTheme(<Container />);
    expect(styledContainer).toHaveStyleRule('padding', theme.spacing.md);
    const styledSeparator = mountWithTheme(<Separator />);
    expect(styledSeparator).toHaveStyleRule('margin', `${theme.spacing.sm} 0 ${theme.spacing.md}`);
  });
});
