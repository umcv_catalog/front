import Divider from '@mui/material/Divider';
import styled from 'styled-components';

export const Container = styled.div`
  padding: ${({ theme }) => theme.spacing.md};
  max-width: 450px;
  margin: 0 auto;
`;

export const Separator = styled(Divider)`
  margin: ${({ theme }) => `${theme.spacing.sm} 0 ${theme.spacing.md}`};
`;
