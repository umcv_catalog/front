import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Button from '@mui/material/Button';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import withStore from 'Hoc/withStore';
import { observer } from 'mobx-react-lite';
import React, { useEffect, useRef } from 'react';
import { AuthService } from 'Services/auth';
import { LOGIN_STATUS } from 'Specs/login-status';
import AbstractAuthStore from 'Specs/stores/authStore';
import authStore from 'Stores/authStore';

import * as Style from './style';

interface ILoginProps {
  auth: AbstractAuthStore;
}

const Login: React.FC<ILoginProps> = observer(({ auth }): React.ReactElement => {
  const authService = new AuthService();
  const { login, password } = auth.loginInfo;
  const passwordRef = useRef<HTMLInputElement>(null);
  const loginRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    if (!login.value) {
      // eslint-disable-next-line no-unused-expressions
      loginRef.current?.focus();
    } else if (!password.value) {
      // eslint-disable-next-line no-unused-expressions
      passwordRef.current?.focus();
    }
  }, []);

  function handleChangeLogin(ev: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void {
    authService.updateLoginField('login', ev.target.value);
  }

  function handleChangePassword(ev: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void {
    authService.updateLoginField('password', ev.target.value);
  }

  function handleLoginClick() {
    authService.logIn();
  }

  function handleLoginKeyUp(ev: React.KeyboardEvent): void {
    if (ev.key === 'Enter') {
      // eslint-disable-next-line no-unused-expressions
      passwordRef.current?.focus();
    }
  }

  function handlePasswordKeyUp(ev: React.KeyboardEvent): void {
    if (ev.key === 'Enter') {
      if (login.value) {
        handleLoginClick();
      } else {
        // eslint-disable-next-line no-unused-expressions
        loginRef.current?.focus();
      }
    }
  }

  return (
    <Style.Container>
      <Typography variant="h5">Sign in</Typography>
      <TextField
        label="Login"
        onChange={handleChangeLogin}
        required
        fullWidth
        margin="normal"
        error={login.isInvalid}
        value={login.value}
        inputRef={loginRef}
        onKeyUp={handleLoginKeyUp}
      />
      <TextField
        label="Password"
        onChange={handleChangePassword}
        required
        fullWidth
        type="password"
        margin="normal"
        error={password.isInvalid}
        value={password.value}
        inputRef={passwordRef}
        onKeyUp={handlePasswordKeyUp}
      />
      {auth.errorMessage && (
        <Alert severity="error">
          <AlertTitle>Error</AlertTitle>
          {auth.errorMessage}
        </Alert>
      )}
      <Style.Separator />
      <Button
        variant="contained"
        color="primary"
        disabled={auth.status === LOGIN_STATUS.PENDING}
        fullWidth
        size="large"
        onClick={handleLoginClick}>
        {auth.status === LOGIN_STATUS.PENDING ? <CircularProgress /> : 'Sign in'}
      </Button>
    </Style.Container>
  );
});

Login.displayName = 'LoginForm';

export default withStore(Login, { auth: authStore });
