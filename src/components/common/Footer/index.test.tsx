import 'jest-styled-components';

import { shallow } from 'enzyme';
import { mountWithTheme } from 'Hoc/withTheme';
import React from 'react';
import { Theme } from 'Root/theme';

import Footer from '.';
import { Block, Container, Footer as StyledFooter } from './style';

describe('Footer testing', () => {
  it('Render Footer', () => {
    const component = shallow(<Footer />);
    expect(component.exists('Footer')).toBeTruthy();
  });

  it('Render Styled elements', () => {
    const theme = new Theme();
    const styledFooter = mountWithTheme(<StyledFooter />);
    expect(styledFooter.find('Footer')).toHaveStyleRule('padding-top', theme.spacing.md);
    const styledContainer = mountWithTheme(<Container />);
    expect(styledContainer).toHaveStyleRule('margin', `0 ${theme.spacing.md}`);
    const styledBlock = mountWithTheme(<Block />);
    expect(styledBlock).toHaveStyleRule('margin', `0 0 ${theme.spacing.md}`);
  });
});
