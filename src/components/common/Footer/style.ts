import styled from 'styled-components';

export const Footer = styled.footer`
  padding-top: ${({ theme }) => theme.spacing.md};
  margin-top: auto;
`;
Footer.displayName = 'Footer';

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 0 ${({ theme }) => theme.spacing.md};
`;

export const Block = styled.div`
  margin: 0 0 ${({ theme }) => theme.spacing.md};
`;
