import React from 'react';

import * as Style from './style';

const Footer = (): React.ReactElement => (
  <Style.Footer>
    <Style.Container>
      <Style.Block>&copy; Personal</Style.Block>
      <Style.Block>All rigths recerved</Style.Block>
    </Style.Container>
  </Style.Footer>
);

export default Footer;
