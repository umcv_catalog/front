import 'jest-styled-components';

import { mountWithTheme } from 'Hoc/withTheme';
import React from 'react';
import { Theme } from 'Root/theme';

import { Container } from './style';

describe('SummaryInfo testing', () => {
  it('Render Styled elements', () => {
    const theme = new Theme();
    const styledContainer = mountWithTheme(<Container />);
    expect(styledContainer).toHaveStyleRule('padding', theme.spacing.md);
  });
});
