import CircularProgress from '@mui/material/CircularProgress';
import React from 'react';

import * as Style from './style';

const SiteLoader: React.FC = () => (
  <Style.Container>
    <CircularProgress size={100} />
  </Style.Container>
);

SiteLoader.displayName = 'SiteLoader';

export default SiteLoader;
