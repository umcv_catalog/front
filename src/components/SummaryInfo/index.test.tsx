import 'jest-styled-components';

import { shallow } from 'enzyme';
import { mountWithTheme } from 'Hoc/withTheme';
import React from 'react';
import { Theme } from 'Root/theme';
import { LOGIN_STATUS } from 'Specs/login-status';
import authStore from 'Stores/authStore';
import summaryInfoStore from 'Stores/summaryInfoStore';

import SummaryInfo from '.';
import { Container } from './style';

describe('SummaryInfo testing', () => {
  it('Render not logged', () => {
    authStore.setStatus(LOGIN_STATUS.NOTLOGGEDIN);
    const component = shallow(<SummaryInfo auth={authStore} summaryInfo={summaryInfoStore} />);
    expect(component.exists('ForwardRef(Alert)')).toBeTruthy();
  });

  it('Render data waiting', () => {
    authStore.setStatus(LOGIN_STATUS.DEFAULT);
    const component = shallow(<SummaryInfo auth={authStore} summaryInfo={summaryInfoStore} />);
    expect(component.exists('SiteLoader')).toBeTruthy();
    authStore.setStatus(LOGIN_STATUS.LOGGEDIN);
    const componentLogged = shallow(<SummaryInfo auth={authStore} summaryInfo={summaryInfoStore} />);
    expect(componentLogged.exists('SiteLoader')).toBeTruthy();
  });

  it('Render full info', () => {
    authStore.setStatus(LOGIN_STATUS.LOGGEDIN);
    summaryInfoStore.setItemsCountInfo({ all: 50, existed: 32, notViewed: 30 }, { all: 102, existed: 65, notViewed: 30 });
    const component = shallow(<SummaryInfo auth={authStore} summaryInfo={summaryInfoStore} />);
    expect(component.exists('ForwardRef(TableContainer)')).toBeTruthy();
  });

  it('Render error message', () => {
    authStore.setStatus(LOGIN_STATUS.LOGGEDIN);
    summaryInfoStore.setErrorMessage('test');
    const component = shallow(<SummaryInfo auth={authStore} summaryInfo={summaryInfoStore} />);
    expect(component.exists('ForwardRef(TableCell)[colSpan]')).toBeTruthy();
  });

  it('Render Styled elements', () => {
    const theme = new Theme();
    const styledContainer = mountWithTheme(<Container />);
    expect(styledContainer).toHaveStyleRule('padding', theme.spacing.md);
  });
});
