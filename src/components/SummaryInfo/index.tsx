import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import SiteLoader from 'Components/common/SiteLoader';
import { observer } from 'mobx-react-lite';
import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { LOGIN_STATUS } from 'Specs/login-status';
import AbstractAuthStore from 'Specs/stores/authStore';
import AbstractSummaryInfoStore from 'Specs/stores/summaryInfo';

import * as Style from './style';

interface ISummaryInfoProps {
  auth: AbstractAuthStore;
  summaryInfo: AbstractSummaryInfoStore;
}

const SummaryInfo: React.FC<ISummaryInfoProps> = observer(({ auth, summaryInfo }) => {

  if (auth.status === LOGIN_STATUS.NOTLOGGEDIN) {
    return (
      <Style.Container>
        <Alert severity="info">
          <AlertTitle>Info</AlertTitle>
          You need be logged in to view summary info
        </Alert>
      </Style.Container>
    );
  }

  if (auth.status !== LOGIN_STATUS.LOGGEDIN || !summaryInfo.loaded) {
    return (
      <SiteLoader />
    );
  }

  return (
    <Style.Container>
      <TableContainer>
        <Table sx={{ maxWidth: 500 }}>
          <TableHead>
            <TableRow>
              <TableCell />
              <TableCell>All count</TableCell>
              <TableCell>Existed count</TableCell>
              <TableCell>Not viewed count</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell component="th" scope="row">
                <Link component={RouterLink} to="/catalog">
                  Catalog
                </Link>
              </TableCell>
              {summaryInfo.errorMessage ? (
                <TableCell align="center" colSpan={3}>{summaryInfo.errorMessage}</TableCell>
              ) : (
                <>
                  <TableCell align="center">{summaryInfo.catalog?.all}</TableCell>
                  <TableCell align="center">{summaryInfo.catalog?.existed}</TableCell>
                  <TableCell align="center">{summaryInfo.catalog?.notViewed}</TableCell>
                </>
              )}
            </TableRow>
            <TableRow>
              <TableCell component="th" scope="row">Videos</TableCell>
              {summaryInfo.errorMessage ? (
                <TableCell align="center" colSpan={3}>{summaryInfo.errorMessage}</TableCell>
              ) : (
                <>
                  <TableCell align="center">{summaryInfo.videos?.all}</TableCell>
                  <TableCell align="center">{summaryInfo.videos?.existed}</TableCell>
                  <TableCell align="center">{summaryInfo.videos?.notViewed}</TableCell>
                </>
              )}
            </TableRow>
          </TableBody>
        </Table>
      </TableContainer>
    </Style.Container>
  );
});

SummaryInfo.displayName = 'SummaryInfo';

export default SummaryInfo;
