import 'jest-styled-components';

import { shallow } from 'enzyme';
import { mountWithTheme } from 'Hoc/withTheme';
import React from 'react';
import { Theme } from 'Root/theme';
import catalogStore from 'Stores/catalogStore';
import { exampleColumns } from 'Stores/catalogStore.test';

import ColumnsSelectorDialog from '.';
import { DialogContent, Separator } from './style';

// eslint-disable-next-line func-names
jest.mock('Services/catalog', () => function () {
  return { setAvailableColumns: () => new Promise<void>(resolve => resolve()) };
});

describe('ColumnsSelectorDialog testing', () => {
  it('Render ColumnsSelectorDialog', () => {
    const component = shallow(<ColumnsSelectorDialog catalog={catalogStore} onClose={jest.fn()} />);
    expect(component.exists('Footer')).toBeTruthy();
  });

  it('Render error message and close', () => {
    const onClose = jest.fn();
    catalogStore.setErrorMessage('test');
    const component = shallow(<ColumnsSelectorDialog catalog={catalogStore} onClose={onClose} />);
    expect(component.exists('ForwardRef(Alert)')).toBeTruthy();
    const button = component.find('ForwardRef(Button)').first();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    button.prop('onClick')();
    expect(onClose).toHaveBeenCalled();
  });

  it('Change and save', async() => {
    const onClose = jest.fn();
    catalogStore.setCurrentColumns(exampleColumns);
    catalogStore.setErrorMessage(null);
    const component = shallow(<ColumnsSelectorDialog catalog={catalogStore} onClose={onClose} />);
    expect(component.exists('ForwardRef(Alert)')).toBeFalsy();
    expect(component.exists('ControlLabel')).toBeTruthy();
    // console.log(component.debug());
    // const checkbox = component.find('ForwardRef(Checkbox)').first();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    // checkbox.prop('onChange')();
    const button = component.find('ForwardRef(Button)').last();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    await button.prop('onClick')();
    expect(onClose).toHaveBeenCalled();
  });

  it('Render Styled elements', () => {
    const theme = new Theme();
    const styledDialogContent = mountWithTheme(<DialogContent />);
    expect(styledDialogContent).toHaveStyleRule('padding', theme.spacing.md);
    const styledSeparator = mountWithTheme(<Separator />);
    expect(styledSeparator).toHaveStyleRule('margin', `${theme.spacing.sm} 0 ${theme.spacing.md}`);
  });
});
