import Divider from '@mui/material/Divider';
import FormControlLabel from '@mui/material/FormControlLabel';
import styled from 'styled-components';

export const DialogContent = styled.div`
  padding: ${({ theme }) => theme.spacing.md};
  margin: 0 auto;
`;

export const Separator = styled(Divider)`
  margin: ${({ theme }) => `${theme.spacing.sm} 0 ${theme.spacing.md}`};
`;

export const Footer = styled.div`
  display: flex;
  justify-content: space-between;
`;

Footer.displayName = 'Footer';

export const ControlLabel = styled(FormControlLabel)`
  width: 180px;
`;
ControlLabel.displayName = 'ControlLabel';
