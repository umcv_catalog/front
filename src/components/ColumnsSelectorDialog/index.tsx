import CancelIcon from '@mui/icons-material/Cancel';
import SaveIcon from '@mui/icons-material/Save';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import CircularProgress from '@mui/material/CircularProgress';
import { observer } from 'mobx-react-lite';
import React, { useCallback, useState } from 'react';
import { CatalogModel } from 'Root/specs/models';
import CatalogService from 'Services/catalog';
import AbstractCatalogStore from 'Specs/stores/catalogStore';

import * as Styled from './style';

interface IColumnsSelectorDialogsProps {
  catalog: AbstractCatalogStore;
  onClose: () => void;
}

const columns: { id: keyof CatalogModel, label: string }[] = [
  { id: 'isViewed', label: 'Viewed' },
  { id: 'id', label: 'ID' },
  { id: 'title', label: 'Title' },
  { id: 'category', label: 'Category' },
  { id: 'genre', label: 'Genre' },
  { id: 'existence', label: 'Existence' },
  { id: 'size', label: 'Size' },
  { id: 'rating', label: 'Rating' },
  { id: 'isHd', label: 'HD' },
  { id: 'isFullHd', label: 'FullHd' },
  { id: 'is4k', label: '4k' },
  { id: 'audioLang', label: 'Audio' },
  { id: 'subtitleLang', label: 'Subtitle' },
  { id: 'summaryDuration', label: 'Duration' },
  { id: 'fileName', label: 'Filename' },
  { id: 'path', label: 'Path' },
  { id: 'dateDownload', label: 'Downloaded' },
  { id: 'dateChanged', label: 'Changed' },
  { id: 'dateViewed', label: 'Viewed' },
  { id: 'source', label: 'Source' },
  { id: 'description', label: 'Description' },
  { id: 'year', label: 'Year' },
];

const ColumnsSelectorDialog: React.FC<IColumnsSelectorDialogsProps> = observer(({ catalog, onClose }) => {
  const [columnsInfoSaving, setColumnsInfoSaving] = useState(false);
  const catalogService = new CatalogService();
  const handleCancelClick = useCallback(() => {
    onClose();
  }, []);

  const handleSaveClick = useCallback(() => {
    setColumnsInfoSaving(true);
    catalogService.setAvailableColumns()
      .then(() => {
        onClose();
        catalog.resetList();
      })
      .finally(() => {
        setColumnsInfoSaving(false);
      });
  }, []);

  return (
    <Styled.DialogContent>
      {columns.map(column => (
        <Styled.ControlLabel
          key={column.id}
          control={(
            <Checkbox
              checked={!!catalog.currentColumns && catalog.currentColumns[column.id]}
              onChange={(_ev, checked) => catalog.setCurrentColumnStatus(column.id, checked)}
            />
          )}
          label={column.label}
        />
      ))}
      {catalog.errorMessage && (
        <Alert severity="error">
          <AlertTitle>Error</AlertTitle>
          {catalog.errorMessage}
        </Alert>
      )}
      <Styled.Separator />
      <Styled.Footer>
        <Button
          variant="outlined"
          color="primary"
          size="large"
          onClick={handleCancelClick}
          startIcon={<CancelIcon />}>
          Cancel
        </Button>
        <Button
          variant="contained"
          color="primary"
          size="large"
          onClick={handleSaveClick}
          startIcon={<SaveIcon />}>
          {columnsInfoSaving ? <CircularProgress /> : 'Save'}
        </Button>
      </Styled.Footer>
    </Styled.DialogContent>
  );
});

ColumnsSelectorDialog.displayName = 'ColumnsSelectorDialog';

export default ColumnsSelectorDialog;
