import 'jest-styled-components';

import { shallow } from 'enzyme';
import { mountWithTheme } from 'Hoc/withTheme';
import React from 'react';
import { Theme } from 'Root/theme';
import { CatalogModel } from 'Specs/models';
import catalogStore from 'Stores/catalogStore';
import { exampleCatalogItem, exampleCatalogItemInverted } from 'Stores/catalogStore.test';

import CatalogList from '.';
import { Cell } from './style';

// eslint-disable-next-line func-names
jest.mock('Services/catalog', () => function () {
  return {
    getListChunkInfo: () => new Promise<CatalogModel[]>(resolve => resolve([])),
    setOrder: () => new Promise<CatalogModel[]>(resolve => resolve([]))
  };
});

describe('CatalogList testing', () => {
  it('Render empty CatalogList', () => {
    const component = shallow(<CatalogList catalog={catalogStore} />);
    expect(component.exists('InfiniteScroll')).toBeTruthy();
  });

  it('Render filled CatalogList', () => {
    catalogStore.addListChunk([exampleCatalogItem, exampleCatalogItemInverted]);
    const component = shallow(<CatalogList catalog={catalogStore} />);
    expect(component.exists('InfiniteScroll')).toBeTruthy();
    const scroll = component.find('InfiniteScroll');
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    scroll.prop('loadMore')();

    expect(component.exists('ForwardRef(TableSortLabel)')).toBeTruthy();
    const TableSortLabel = component.find('ForwardRef(TableSortLabel)').first();
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    TableSortLabel.prop('onClick')();
  });

  it('Render Styled elements', () => {
    const theme = new Theme();
    const styledCell = mountWithTheme(<Cell />);
    expect(styledCell).toHaveStyleRule('background-color', 'transparent');
    expect(styledCell).toHaveStyleRule('padding', `0 ${theme.spacing.sm}`);
    const colors = [
      'transparent', '#F29494', '#F2B294', '#F2C694', '#F2DA94', '#F2F194', '#D5F294', '#C1F294', '#AAF294', '#94F2B6', '#94B6F2'
    ];
    for (let rating = 0; rating <= 10; rating++) {
      const styledRatedCell = mountWithTheme(<Cell rating={rating} />);
      expect(styledRatedCell).toHaveStyleRule('background-color', colors[rating]);
    }
  });
});
