import TableCell from '@mui/material/TableCell';
import styled from 'styled-components';

interface IRatingProps {
  rating?: number;
}

export const Cell = styled(TableCell)<IRatingProps>`
  padding: 0 ${({ theme }) => theme.spacing.sm};
  background-color: ${({ rating }) => {
    switch (rating) {
      case 1: return '#F29494';
      case 2: return '#F2B294';
      case 3: return '#F2C694';
      case 4: return '#F2DA94';
      case 5: return '#F2F194';
      case 6: return '#D5F294';
      case 7: return '#C1F294';
      case 8: return '#AAF294';
      case 9: return '#94F2B6';
      case 10: return '#94B6F2';
      default: return 'transparent';
    }
  }};
`;
