/* eslint-disable react/no-multi-comp */
/* eslint-disable react/display-name */
import CoronavirusIcon from '@mui/icons-material/Coronavirus';
import CircularProgress from '@mui/material/CircularProgress';
import Link from '@mui/material/Link';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import { observer } from 'mobx-react-lite';
import React, { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
import { CatalogModel } from 'Root/specs/models';
import { Order } from 'Root/specs/order';
import CatalogService from 'Services/catalog';
import { CatalogColumn } from 'Specs/catalogColumn';
import AbstractCatalogStore from 'Specs/stores/catalogStore';

import * as Style from './style';

interface ICatalogListProps {
  catalog: AbstractCatalogStore;
}

const columns: readonly CatalogColumn[] = [
  {
    id: 'isViewed',
    label: '',
    align: 'center',
    minWidth: 17,
    format: (viewed: boolean) => (viewed ? (<CoronavirusIcon fontSize="small" />) : '')
  },
  { id: 'id', label: 'ID', align: 'center', minWidth: 17 },
  { id: 'title', label: 'Title' },
  { id: 'category', label: 'Category' },
  { id: 'genre', label: 'Genre', format: (genres: (string | null)[]) => genres.filter(genre => !!genre).join(', ') },
  {
    id: 'existence',
    label: 'Ex',
    align: 'center',
    minWidth: 17,
    format: (existence: boolean) => (existence ? (<CoronavirusIcon fontSize="small" />) : '')
  },
  { id: 'size', label: 'Size', format: (size: number) => size.toFixed(2) },
  { id: 'rating', label: 'R', align: 'center', minWidth: 17, format: (rating: number) => (rating || '') },
  {
    id: 'isHd',
    label: 'HD',
    align: 'center',
    minWidth: 17,
    format: (isHd: boolean) => (isHd ? (<CoronavirusIcon fontSize="small" />) : '')
  },
  {
    id: 'isFullHd',
    label: 'FullHd',
    align: 'center',
    minWidth: 17,
    format: (isFullHd: boolean) => (isFullHd ? (<CoronavirusIcon fontSize="small" />) : '')
  },
  {
    id: 'is4k',
    label: '4k',
    align: 'center',
    minWidth: 17,
    format: (is4k: boolean) => (is4k ? (<CoronavirusIcon fontSize="small" />) : '')
  },
  { id: 'audioLang', label: 'Audio', format: (langs: (string | null)[]) => langs.filter(lang => !!lang).join(', ') },
  { id: 'subtitleLang', label: 'Sub', format: (langs: (string | null)[]) => langs.filter(lang => !!lang).join(', ') },
  { id: 'summaryDuration', label: 'Duration' },
  { id: 'fileName', label: 'Filename' },
  { id: 'path', label: 'Path' },
  { id: 'dateDownload', label: 'Downloaded' },
  { id: 'dateChanged', label: 'Changed' },
  { id: 'dateViewed', label: 'Viewed' },
  {
    id: 'source',
    label: 'Source',
    format: ({ name, link }: {name: string, link: string}) => (<Link href={link} target="_blank">{name}</Link>)
  },
  { id: 'description', label: 'Description' },
  { id: 'year', label: 'Year' },
];

const CatalogList: React.FC<ICatalogListProps> = observer(({ catalog }) => {
  const [hasNextPage, setHasNextPage] = useState(true);
  const [waitSortingUpdate, setWaitSortingUpdate] = useState(false);
  const catalogService = new CatalogService();

  function loadMore() {
    catalogService.getListChunkInfo()
      .then(list => {
        if (!list.length) {
          setHasNextPage(false);
        }
      });
  }

  const createSortHandler = (property: keyof CatalogModel) => () => {
    setWaitSortingUpdate(true);
    catalogService.setOrder(property)
      .finally(() => {
        setWaitSortingUpdate(false);
      });
  };

  return (
    <InfiniteScroll
      loadMore={loadMore}
      hasMore={hasNextPage && !waitSortingUpdate}
      loader={<CircularProgress key="loader" />}>
      <TableContainer>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              {columns.map(column => (catalog.viewColumns.includes(column.id) ? (
                <Style.Cell
                  key={column.id}
                  align="center"
                  padding="none"
                  style={{ minWidth: column.minWidth }}>
                  <TableSortLabel
                    active={catalog.orderBy === column.id}
                    direction={catalog.orderBy === column.id ? catalog.order : Order.ASC}
                    onClick={createSortHandler(column.id)}>
                    {column.label}
                  </TableSortLabel>
                </Style.Cell>
              ) : null))}
            </TableRow>
          </TableHead>
          <TableBody>
            {catalog.list && catalog.list
              .map(row => (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                  {columns.map(column => {
                    const value = row[column.id];
                    return (catalog.viewColumns.includes(column.id) ? (
                      <Style.Cell
                        key={column.id}
                        align={column.align}
                        padding="none"
                        rating={column.id === 'rating' ? value as number : 0}>
                        {column.format ? column.format(value) : value}
                      </Style.Cell>
                    ) : null);
                  })}
                </TableRow>
              ))}
          </TableBody>
        </Table>
      </TableContainer>
    </InfiniteScroll>
  );
});

CatalogList.displayName = 'CatalogList';

export default CatalogList;
