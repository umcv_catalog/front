import 'styled-components';

import { Intent } from 'Specs/intent';
import { Size } from 'Specs/size';

type Color = {
  Main: {
    AccentMain: string,
    AccentAdditional: string,
    Bill: string,
    Chips: string,
    InactiveSelection: string,
    Neutral: string,
  },
  Background: {
    Subheader: string,
    BluredBg: string,
    WebPage: string,
  },
  Divider: {
    Dark: string,
    Light: string,
  },
  Text: {
    TitleDark: string,
    MainTextDark: string,
    White: string
  }
}

declare module 'styled-components' {
  export interface DefaultTheme {
    animationDuration: string;
    borderRadius: string;
    color: Color;
    lineHeight: string;
    intentColor: {
      [keys in Intent]: string;
    };
    fontSize: {
      [keys in Size]: string;
    };
    spacing: {
      [keys in Size]: string;
    };
    screenSize: {
      [keys in Size]: string;
    }
  }
}
