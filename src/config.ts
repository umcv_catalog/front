const config = {
  apiUrl: 'http://localhost:3500/',
  routePath: {
    ROOT: '/',

    CATALOG: '/catalog',
    CATALOG_EDIT: '/catalog/edit',
    CATALOG_NEW: '/catalog/new',

    LOGIN: '/login'
  }
};

export default config;
export declare type Config = typeof config;
