export const Size = {
  LG: 'lg',
  MD: 'md',
  SM: 'sm',
  XL: 'xl',
  XS: 'xs',
} as const;

export type Size = typeof Size[keyof typeof Size];
