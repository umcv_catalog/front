export const Intent = {
  DANGER: 'danger',
  DEFAULT: 'default',
  INFO: 'info',
  NONE: 'none',
  PRIMARY: 'primary',
  SUCCESS: 'success',
  WARNING: 'warning',
} as const;

export type Intent = typeof Intent[keyof typeof Intent];
