import { LOGIN_STATUS } from 'Specs/login-status';
import { LoginInfoModel, UserModel } from 'Specs/models';

export default abstract class AbstractAuthStore {

  token!: string | null;

  loginInfo!: LoginInfoModel;

  user!: UserModel | null;

  status!: LOGIN_STATUS;

  errorMessage!: string | null;

  isLoginDialogOpened!: boolean;

  abstract get isAuthorized(): boolean;

  abstract setUser(user: UserModel | null): void;

  abstract setToken(token: string | null): void;

  abstract setLoginInfo(info: LoginInfoModel): void;

  abstract setStatus(status: LOGIN_STATUS): void;

  abstract setErrorMessage(errorMessage: string | null): void;

  abstract setLoginDialogOpened(isOpened: boolean): void;

}
