import { CatalogModel, TAvailableColumns } from 'Specs/models';
import { Order } from 'Specs/order';

export default abstract class AbstractCatalogStore {

  list!: CatalogModel[] | null;

  orderBy!: keyof CatalogModel;

  order!: Order;

  page!: number;

  errorMessage!: string | null;

  viewColumns!: (keyof CatalogModel)[];

  availableColumns!: TAvailableColumns | null;

  currentColumns!: TAvailableColumns | null;

  abstract resetList(): void;

  abstract addListChunk(items: CatalogModel[]): void;

  abstract setErrorMessage(errorMessage: string | null): void;

  abstract setOrderBy(orderBy: keyof CatalogModel): void;

  abstract setOrder(order: Order): void;

  abstract setAvailableColumns(columns: TAvailableColumns): void;

  abstract setCurrentColumns(columns: TAvailableColumns): void;

  abstract setCurrentColumnStatus(column: keyof CatalogModel, status: boolean): void;

}
