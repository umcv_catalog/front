import { ItemsCount } from 'Specs/models';

export default abstract class AbstractSummaryInfoStore {

  catalog!: ItemsCount | null;

  videos!: ItemsCount | null;

  errorMessage!: string | null;

  abstract get loaded(): boolean;

  abstract setItemsCountInfo(catalog: ItemsCount, videos: ItemsCount): void;

  abstract setErrorMessage(errorMessage: string | null): void;

}
