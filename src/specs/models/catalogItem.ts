export class CatalogModel {

  id?: number;
  objID!: number;
  title?: string;
  fileName?: string;
  category?: string;
  genre?: string[];
  size?: number;
  existence?: boolean;
  rating?: number;
  dateDownload?: string;
  dateViewed?: string | null;
  dateChanged?: string;
  source?: {
    name: string;
    link: string;
  };

  path?: string;
  description?: string;
  isHd?: boolean;
  isFullHd?: boolean;
  is4k?: boolean;
  audioLang?: (string | null)[];
  subtitleLang?: (string | null)[];
  summaryDuration?: string;
  year?: string;
  countVideo?: number;
  isViewed?: boolean;
  isCompleted?: boolean;

}

export type TAvailableColumns = Record<keyof CatalogModel, boolean>;
