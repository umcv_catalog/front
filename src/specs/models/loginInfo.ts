export class LoginInfoModel {

  login!: {
    value: string;
    isInvalid: boolean;
    isValid: boolean;
  }

  password!: {
    value: string;
    isInvalid: boolean;
    isValid: boolean;
  }

}
