import React from 'react';

import { CatalogModel } from './models';

export interface CatalogColumn {
  id: keyof CatalogModel;
  label: string;
  minWidth?: number;
  align?: 'right' | 'left' | 'center';
  format?: (value: any) => React.ReactNode;
}
