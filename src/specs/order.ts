export const Order = {
  ASC: 'asc',
  DESC: 'desc',
} as const;

export type Order = typeof Order[keyof typeof Order];
