import express, { Request, Response } from 'express';
import React from 'react';
import ReactDOMServer from 'react-dom/server';
import { StaticRouter } from 'react-router';
import { ServerStyleSheet } from 'styled-components';

/* eslint-disable import/no-unresolved */
import stats from '../dist/renderer/stats.json';
import webpackDevConfig from '../webpack/development.webpack';
import devServer from '../webpack/devserver.webpack';
import webpackProdConfig from '../webpack/production.webpack';
import App from './App';
import { Html } from './html/server';

const port = 3000;
const server = express();
server.disable('x-powered-by');
const isDev = process.env.NODE_ENV === 'development';
const scripts: string[] = [];
const stylesheets: string[] = [];

if (webpackDevConfig.output && webpackProdConfig.output) {
  const { publicPath } = isDev ? webpackDevConfig.output : webpackProdConfig.output;
  if (Array.isArray(stats.assetsByChunkName.main)) {
    stats.assetsByChunkName.main.forEach((script: string) => {
      if (script.split('.').slice(-1)[0] === 'js') {
        scripts.push(`${publicPath}${script}`);
      }
    });
  } else if (stats.assetsByChunkName.main) {
    scripts.push(`${publicPath}${stats.assetsByChunkName.main}`);
  }
}

server.use('/', express.static('./dist/renderer'));

server.get('*', async(req: Request, res: Response) => {
  const sheet = new ServerStyleSheet();
  const body = sheet.collectStyles(<App />);
  const styles = sheet.getStyleTags();
  ReactDOMServer.renderToNodeStream(
    <Html scripts={scripts} stylesheets={stylesheets} styles={styles} title="Video catalog">
      <StaticRouter location={req.url} context={{}}>
        {body}
      </StaticRouter>
    </Html>
  ).pipe(res);
});

server.listen(port, () => {
  console.log(`Listening on port ${port}`);
  if (isDev) {
    devServer();
  }
});
