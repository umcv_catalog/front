import axios, { AxiosResponse, Method } from 'axios';
import extend from 'lodash/extend';
import qs from 'qs';

class ApiError extends Error {

  name: string;

  message: string;

  code?: string | number | null;

  pid?: string | null;

  data?: Record<string, unknown>;

  success?: boolean;

  constructor(message: string, code: number, pid: string, data: Record<string, unknown>) {
    super();
    this.name = 'ApiError';
    this.message = message || 'Api error';
    this.code = code || null;
    this.pid = pid || null;
    this.data = data || {};
  }

}

// ApiError.prototype = Error.prototype;

export interface IRequest {
  data: any;
  url: string;
  urlPath: string;
  method?: Method;
  withCredentials?: boolean;
}

/** Базовый API запрос */
export async function request<T>({
  data,
  urlPath = '',
  method = 'get',
  url = '',
  withCredentials = false,
}: IRequest): Promise<AxiosResponse<T>> {

  const requestUrl = `${url}${urlPath}`;

  const req = axios({
    url: `${requestUrl}${method === 'get' ? `?${new URLSearchParams(qs.stringify(data))}` : ''}`,
    method,
    data: method === 'post' ? new URLSearchParams(qs.stringify(data)) : null,
    responseType: 'json',
    withCredentials,
  });

  const response = await req;
  const respData = response.data;

  if (respData && respData.success === false) {
    throw new ApiError(
      respData.message || (respData.messages && respData.messages[0]) || (respData.data && respData.data.message),
      respData.code,
      respData.pid,
      respData.data
    );
  }

  return respData;
}

/** GET запрос */
export function get<T>(data: IRequest): Promise<AxiosResponse<T>> {
  return request(extend(data, { method: 'get' }));
}

/** POST запрос */
export function post<T>(data: IRequest): Promise<AxiosResponse<T>> {
  return request(extend(data, { method: 'post' }));
}
