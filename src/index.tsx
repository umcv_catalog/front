import { createBrowserHistory } from 'history';
import React from 'react';
import { hydrate, render } from 'react-dom';
import { Router } from 'react-router';

import App from './App';

let mainElement = document.getElementById('root');

const history = createBrowserHistory();
const jsx = (
  <Router history={history}>
    <App />
  </Router>
);

if (mainElement) {
  hydrate(jsx, mainElement);
} else {
  mainElement = document.createElement('div');
  mainElement.setAttribute('id', 'root');
  document.body.appendChild(mainElement);
  render(jsx, mainElement);
}
