import { action, computed, makeObservable, observable } from 'mobx';
import { makePersistable } from 'mobx-persist-store';
import canUseDOM from 'Root/utils/canUseDom';
import { LOGIN_STATUS } from 'Specs/login-status';
import { LoginInfoModel, UserModel } from 'Specs/models';
import AbstractAuthStore from 'Specs/stores/authStore';

class AuthStore implements AbstractAuthStore {

  constructor() {
    makeObservable(this);
    makePersistable(
      this,
      {
        storage: canUseDOM ? window.localStorage : undefined,
        name: 'AuthStore',
        properties: ['token', 'user'],
      }
    );
  }

  @observable
  public token: string | null = null;

  @observable
  public loginInfo: LoginInfoModel = {
    login: {
      value: '',
      isInvalid: false,
      isValid: false,
    },
    password: {
      value: '',
      isInvalid: false,
      isValid: false,
    },
  };

  @observable.ref
  public user: UserModel | null = null;

  @observable
  public status: LOGIN_STATUS = LOGIN_STATUS.DEFAULT;

  @observable
  public errorMessage: string | null = null;

  @observable
  public isLoginDialogOpened = false;

  @computed
  get isAuthorized(): boolean {
    return this.status === LOGIN_STATUS.LOGGEDIN;
  }

  @action
  setUser(user: UserModel | null): void {
    this.user = user;
    this.status = user ? LOGIN_STATUS.LOGGEDIN : LOGIN_STATUS.NOTLOGGEDIN;
    this.errorMessage = null;
  }

  @action
  setToken(token: string | null): void {
    this.token = token;
  }

  @action
  setLoginInfo(info: LoginInfoModel): void {
    this.loginInfo = info;
  }

  @action
  setStatus(status: LOGIN_STATUS): void {
    this.status = status;
  }

  @action
  setErrorMessage(errorMessage: string | null): void {
    this.errorMessage = errorMessage;
  }

  @action
  setLoginDialogOpened(isOpened: boolean): void {
    this.isLoginDialogOpened = isOpened;
  }

}

export default new AuthStore();
