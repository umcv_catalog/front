import { CatalogModel, TAvailableColumns } from 'Specs/models';
import { Order } from 'Specs/order';

import catalogStore from './catalogStore';

export const exampleCatalogItem: CatalogModel = {
  id: 2716,
  objID: 182685589,
  title: 'Test title',
  fileName: 'test_file_name.mkv',
  category: 'cat1',
  genre: ['genre1'],
  size: 20.1,
  existence: true,
  rating: 0,
  dateDownload: '2020-02-10T22:00:00.000Z',
  dateViewed: null,
  dateChanged: '2020-02-10T22:00:00.000Z',
  source: {
    name: 'TEST_SOURCE',
    link: 'https://test',
  },
  path: 'c:\\',
  description: '',
  isHd: true,
  isFullHd: false,
  is4k: false,
  audioLang: ['eng'],
  subtitleLang: [null],
  summaryDuration: '08:03:47',
  year: '2020',
  countVideo: 9,
  isViewed: false,
  isCompleted: true,
};

export const exampleCatalogItemInverted: CatalogModel = {
  id: 1,
  objID: 182685589,
  title: 'Test title',
  fileName: 'test_file_name.mkv',
  category: 'cat1',
  genre: ['genre1'],
  size: 20.1,
  existence: false,
  rating: 10,
  dateDownload: '2020-02-10T22:00:00.000Z',
  dateViewed: null,
  dateChanged: '2020-02-10T22:00:00.000Z',
  source: {
    name: 'TEST_SOURCE',
    link: 'https://test',
  },
  path: 'c:\\',
  description: '',
  isHd: false,
  isFullHd: true,
  is4k: true,
  audioLang: [null],
  subtitleLang: ['eng'],
  summaryDuration: '08:03:47',
  year: '2020',
  countVideo: 0,
  isViewed: true,
  isCompleted: false,
};

export const exampleColumns: TAvailableColumns = {
  id: false,
  objID: true,
  title: false,
  fileName: false,
  category: false,
  genre: false,
  size: false,
  existence: false,
  rating: false,
  dateDownload: false,
  dateViewed: true,
  dateChanged: true,
  source: false,
  path: false,
  description: false,
  isHd: false,
  isFullHd: true,
  is4k: true,
  audioLang: false,
  subtitleLang: false,
  summaryDuration: false,
  year: false,
  countVideo: true,
  isViewed: false,
  isCompleted: true,
};

describe('Catalog Store testing', () => {
  it('set catalog list', () => {
    catalogStore.addListChunk([exampleCatalogItem]);
    expect(catalogStore.list?.length).toBe(1);

    catalogStore.addListChunk([exampleCatalogItem]);
    expect(catalogStore.list?.length).toBe(2);

    catalogStore.resetList();
    expect(catalogStore.list).toBeNull();
  });

  it('set error message', () => {
    catalogStore.setErrorMessage('test');
    expect(catalogStore.errorMessage).toBe('test');
  });

  it('set sorting params', () => {
    catalogStore.setOrderBy('title');
    expect(catalogStore.orderBy).toBe('title');
    catalogStore.setOrder(Order.ASC);
    expect(catalogStore.order).toBe(Order.ASC);
  });

  it('set columns', () => {
    catalogStore.setAvailableColumns(exampleColumns);
    expect(catalogStore.availableColumns).toStrictEqual(exampleColumns);
    catalogStore.setCurrentColumnStatus('id', false);
    expect(catalogStore.currentColumns).toBeNull();
    catalogStore.setCurrentColumns(exampleColumns);
    expect(catalogStore.currentColumns).toStrictEqual(exampleColumns);
    catalogStore.setCurrentColumnStatus('id', false);
    expect(catalogStore.currentColumns?.id).toBeFalsy();
  });
});
