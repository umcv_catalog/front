import { action, makeObservable, observable } from 'mobx';
import { makePersistable } from 'mobx-persist-store';
import canUseDOM from 'Root/utils/canUseDom';
import { CatalogModel, TAvailableColumns } from 'Specs/models';
import { Order } from 'Specs/order';
import AbstractCatalogStore from 'Specs/stores/catalogStore';

class CatalogStore implements AbstractCatalogStore {

  constructor() {
    makeObservable(this);
    makePersistable(
      this,
      {
        storage: canUseDOM ? window.localStorage : undefined,
        name: 'CatalogStore',
        properties: ['orderBy', 'order'],
      }
    );
  }

  @observable.shallow
  public list: CatalogModel[] | null = null;

  @observable
  public page = 0;

  @observable
  public errorMessage: string | null = null;

  @observable
  public orderBy: keyof CatalogModel = 'id';

  @observable
  public order: Order = Order.DESC;

  public viewColumns: (keyof CatalogModel)[] = [];

  @observable.deep
  public availableColumns: TAvailableColumns | null = null;

  @observable.deep
  public currentColumns: TAvailableColumns | null = null;

  @action
  resetList(): void {
    this.list = null;
    this.page = 0;
  }

  @action
  addListChunk(items: CatalogModel[]): void {
    if (this.list) {
      this.list.push(...items);
      this.page++;
    } else {
      this.list = items;
      this.viewColumns = Object.keys(items[0]) as (keyof CatalogModel)[];
      this.page = 1;
    }
  }

  @action
  setErrorMessage(errorMessage: string | null): void {
    this.errorMessage = errorMessage;
  }

  @action
  setOrderBy(orderBy: keyof CatalogModel): void {
    this.orderBy = orderBy;
  }

  @action
  setOrder(order: Order): void {
    this.order = order;
  }

  @action
  setAvailableColumns(columns: TAvailableColumns): void {
    this.availableColumns = columns;
  }

  @action
  setCurrentColumns(columns: TAvailableColumns): void {
    this.currentColumns = columns;
  }

  @action
  setCurrentColumnStatus(column: keyof CatalogModel, status: boolean): void {
    if (this.currentColumns) {
      this.currentColumns[column] = status;
    }
  }

}

export default new CatalogStore();
