import { action, computed, makeObservable, observable } from 'mobx';
import { ItemsCount } from 'Specs/models';
import AbstractSummaryInfoStore from 'Specs/stores/summaryInfo';

class SummaryInfoStore implements AbstractSummaryInfoStore {

  constructor() {
    makeObservable(this);
  }

  @observable
  public catalog: ItemsCount | null = null;

  @observable
  public videos: ItemsCount | null = null;

  @observable
  public errorMessage: string | null = null;

  @computed
  get loaded(): boolean {
    return this.catalog !== null || this.videos !== null || this.errorMessage !== null;
  }

  @action
  setItemsCountInfo(catalog: ItemsCount, videos: ItemsCount): void {
    this.catalog = catalog;
    this.videos = videos;
    this.errorMessage = null;
  }

  @action
  setErrorMessage(errorMessage: string | null): void {
    this.errorMessage = errorMessage;
  }

}

export default new SummaryInfoStore();
