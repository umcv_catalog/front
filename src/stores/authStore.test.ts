import { LOGIN_STATUS } from 'Specs/login-status';

import authStore from './authStore';

describe('Auth Store testing', () => {
  it('login', () => {
    authStore.setUser({
      username: 'user',
      status: '0',
      role: 'user',
    });
    expect(authStore.isAuthorized).toBeTruthy();
    expect(authStore.user?.username).toBe('user');
    authStore.setToken('test');
    expect(authStore.token).toBe('test');
    authStore.setStatus(LOGIN_STATUS.NOTLOGGEDIN);
    expect(authStore.isAuthorized).toBeFalsy();
  });

  it('loginInfo', () => {
    authStore.setLoginInfo({
      login: { value: 'user', isValid: true, isInvalid: false },
      password: { value: 'test', isValid: true, isInvalid: false },
    });
    expect(authStore.loginInfo?.login.value).toBe('user');
  });
});
