import summaryInfoStore from './summaryInfoStore';

describe('Summary info store testing', () => {
  it('set count info', () => {
    summaryInfoStore.setItemsCountInfo({
      all: 50,
      existed: 32,
      notViewed: 30
    }, {
      all: 102,
      existed: 65,
      notViewed: 30
    });
    expect(summaryInfoStore.errorMessage).toBeFalsy();
    expect(summaryInfoStore.catalog?.all).toBe(50);
    expect(summaryInfoStore.videos?.all).toBe(102);
  });

  it('set error', () => {
    summaryInfoStore.setErrorMessage('test');
    expect(summaryInfoStore.errorMessage).toBe('test');
  });
});
