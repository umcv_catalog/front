import authStore from './authStore';
import catalogStore from './catalogStore';
import summaryInfoStore from './summaryInfoStore';

const Store = {
  authStore,
  catalogStore,
  summaryInfoStore,
};

export {
  Store
};
