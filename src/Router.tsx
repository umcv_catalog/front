import CatalogPage from 'Pages/Catalog';
import LoginPage from 'Pages/Login';
import NotFoundPage from 'Pages/NotFound';
import StartPage from 'Pages/Start';
import React from 'react';
import { Route, Switch } from 'react-router-dom';
import config from 'Root/config';

const { routePath } = config;

const Routes = (): React.ReactElement => (
  <Switch>
    <Route exact path={routePath.ROOT} component={StartPage} />
    <Route exact path={routePath.LOGIN} component={LoginPage} />
    <Route exact path={routePath.CATALOG} component={CatalogPage} />
    <Route path="*" component={NotFoundPage} />
  </Switch>
);

export default Routes;
