import { createGlobalStyle } from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  body {
    font-family: Arial, Helvetica, sans-serif;
    font-size: ${({ theme }) => theme.fontSize.md};
    color: ${({ theme }) => theme.color.Text.MainTextDark};
    margin: 0;
    background-color: ${({ theme }) => theme.color.Background.WebPage};
  }

  #root {
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    padding-top: 56px;
  }
`;
