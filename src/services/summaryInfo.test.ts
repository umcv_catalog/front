import summaryInfoStore from 'Stores/summaryInfoStore';

import { AuthService } from './auth';
import { SummaryInfoService } from './summaryInfo';

describe('Auth Service testing', () => {
  it('logIn', async() => {
    const authService = new AuthService();
    const summaryInfoService = new SummaryInfoService();
    authService.updateLoginField('login', 'testuser_1');
    authService.updateLoginField('password', '12345');
    await authService.logIn();
    await summaryInfoService.getSummaryInfo();
    expect(summaryInfoStore.catalog).not.toBeNull();
    expect(summaryInfoStore.videos).not.toBeNull();
  });

  it('get info error', async() => {
    const authService = new AuthService();
    const summaryInfoService = new SummaryInfoService();
    await authService.logOut();
    await summaryInfoService.getSummaryInfo();
    expect(summaryInfoStore.errorMessage).not.toBeNull();
  });

});
