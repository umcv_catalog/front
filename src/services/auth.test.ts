import { LOGIN_STATUS } from 'Specs/login-status';
import authStore from 'Stores/authStore';

import { AuthService } from './auth';

describe('Auth Service testing', () => {
  it('logIn', async() => {
    const authService = new AuthService();
    authService.updateLoginField('login', 'testuser_1');
    authService.updateLoginField('password', '12345');
    await authService.logIn();
    expect(authStore.user).not.toBeNull();
    expect(authStore.token).not.toBeNull();
  });

  it('logIn error', async() => {
    const authService = new AuthService();
    authService.updateLoginField('login', 'test');
    authService.updateLoginField('password', 'test');
    await authService.logIn();
    expect(authStore.errorMessage).not.toBeNull();
  });

  it('logOut', async() => {
    const authService = new AuthService();
    authService.updateLoginField('login', 'testuser_1');
    authService.updateLoginField('password', '12345');
    await authService.logIn();
    expect(authStore.isAuthorized).toBeTruthy();
    await authService.logOut();
    expect(authStore.isAuthorized).toBeFalsy();
  });

  it('getMe', async() => {
    const authService = new AuthService();
    authService.updateLoginField('login', 'testuser_1');
    authService.updateLoginField('password', '12345');
    await authService.logIn();
    await authService.getMe();
    expect(authStore.isAuthorized).toBeTruthy();
    expect(authStore.user).not.toBeNull();
    expect(authStore.token).not.toBeNull();
  });

  it('getMe error', async() => {
    const authService = new AuthService();
    await authService.logOut();
    await authService.getMe();
    expect(authStore.status).toBe(LOGIN_STATUS.NOTLOGGEDIN);
  });
});
