import { Order } from 'Root/specs/order';
import catalogStore from 'Stores/catalogStore';
import { exampleColumns } from 'Stores/catalogStore.test';

import { AuthService } from './auth';
import CatalogService from './catalog';

describe('Catalog Service testing', () => {
  afterEach(() => {
    catalogStore.setErrorMessage(null);
    catalogStore.setOrderBy('id');
    catalogStore.setOrder(Order.ASC);
    catalogStore.availableColumns = null;
    catalogStore.currentColumns = null;
  });

  it('logged in request', async() => {
    const authService = new AuthService();
    const catalogService = new CatalogService();
    authService.updateLoginField('login', 'testuser_1');
    authService.updateLoginField('password', '12345');
    await authService.logIn();
    await catalogService.getListChunkInfo(1);
    expect(catalogStore.list).not.toBeNull();
    expect(catalogStore.errorMessage).toBeNull();
  });

  it('logged out request', async() => {
    const authService = new AuthService();
    const catalogService = new CatalogService();
    catalogStore.resetList();
    await authService.logOut();
    await catalogService.getListChunkInfo(1);
    expect(catalogStore.list).toBeNull();
    expect(catalogStore.errorMessage).not.toBeNull();
  });

  it('request with custom sort', async() => {
    const catalogService = new CatalogService();
    catalogService.setOrder('title');
    expect(catalogStore.order).toBe(Order.ASC);
    expect(catalogStore.orderBy).toBe('title');
    catalogService.setOrder('title');
    expect(catalogStore.order).toBe(Order.DESC);
  });

  it('get available columns', async() => {
    const authService = new AuthService();
    const catalogService = new CatalogService();
    authService.updateLoginField('login', 'testuser_1');
    authService.updateLoginField('password', '12345');
    await authService.logIn();
    expect(catalogStore.availableColumns).toBeNull();
    await catalogService.getAvailableColumns();
    expect(catalogStore.availableColumns?.id).toBeTruthy();
    expect(catalogStore.errorMessage).toBeNull();
    await authService.logOut();
    await catalogService.getAvailableColumns();
    expect(catalogStore.errorMessage).not.toBeNull();
  });

  it('set available columns', async() => {
    const authService = new AuthService();
    const catalogService = new CatalogService();
    authService.updateLoginField('login', 'testuser_2');
    authService.updateLoginField('password', '12345');
    await authService.logIn();
    await catalogService.setAvailableColumns();
    expect(catalogStore.errorMessage).not.toBeNull();
    catalogStore.setCurrentColumns(exampleColumns);
    await catalogService.setAvailableColumns();
    expect(catalogStore.errorMessage).toBeNull();
    await authService.logOut();
    await catalogService.setAvailableColumns();
    expect(catalogStore.errorMessage).not.toBeNull();
  });
});
