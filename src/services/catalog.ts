import { transaction } from 'mobx';
import config from 'Root/config';
import { Order } from 'Root/specs/order';
import { get, post } from 'Root/utils/api';
import { CatalogModel, TAvailableColumns } from 'Specs/models';
import authStore from 'Stores/authStore';
import catalogStore from 'Stores/catalogStore';

class CatalogService {

  // eslint-disable-next-line class-methods-use-this
  async getListChunkInfo(limit?: number): Promise<CatalogModel[]> {
    try {
      const data = await get<CatalogModel[]>({
        url: config.apiUrl,
        urlPath: 'catalog/list',
        data: {
          auth_token: authStore.token,
          page: catalogStore.page,
          limit,
          order_by: catalogStore.orderBy,
          order: catalogStore.order,
        }
      });
      transaction(() => {
        catalogStore.addListChunk(data.data);
        catalogStore.setErrorMessage(null);
      });
      return data.data;
    } catch (err) {
      catalogStore.setErrorMessage(err.response?.data?.data.message || 'Network error');
    }
    return [];
  }

  setOrder(orderBy: keyof CatalogModel): Promise<CatalogModel[]> {
    transaction(() => {
      catalogStore.setOrder(orderBy === catalogStore.orderBy && catalogStore.order === Order.ASC ? Order.DESC : Order.ASC);
      catalogStore.setOrderBy(orderBy);
      catalogStore.resetList();
    });
    return this.getListChunkInfo();
  }

  // eslint-disable-next-line class-methods-use-this
  async getAvailableColumns(): Promise<TAvailableColumns | null> {
    try {
      const data = await get<TAvailableColumns>({
        url: config.apiUrl,
        urlPath: 'catalog/columns-view',
        data: {
          auth_token: authStore.token,
        }
      });
      transaction(() => {
        catalogStore.setAvailableColumns(data.data);
        catalogStore.setErrorMessage(null);
      });
      return data.data;
    } catch (err) {
      catalogStore.setErrorMessage(err.response?.data?.data.message || 'Network error');
    }
    return null;
  }

  // eslint-disable-next-line class-methods-use-this
  async setAvailableColumns(): Promise<void> {
    const statuses = catalogStore.currentColumns;
    if (!statuses) {
      catalogStore.setErrorMessage('Statuses is empty');
      return;
    }
    try {
      await post({
        url: config.apiUrl,
        urlPath: 'catalog/columns-view',
        data: {
          auth_token: authStore.token,
          statuses,
        }
      });
      catalogStore.setErrorMessage(null);
    } catch (err) {
      catalogStore.setErrorMessage(err.response?.data?.data.message || 'Network error');
    }
  }

}

export default CatalogService;
