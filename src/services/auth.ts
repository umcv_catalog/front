import { transaction } from 'mobx';
import config from 'Root/config';
import { get, post } from 'Root/utils/api';
import { LOGIN_STATUS } from 'Specs/login-status';
import { LoginInfoModel, UserModel } from 'Specs/models';
import authStore from 'Stores/authStore';

export class AuthService {

  constructor() {
    this.getMe = this.getMe.bind(this);
    this.logIn = this.logIn.bind(this);
  }

  async getMe(): Promise<void> {
    if (!authStore.token) {
      authStore.setStatus(LOGIN_STATUS.NOTLOGGEDIN);
      return;
    }
    authStore.setStatus(LOGIN_STATUS.PENDING);
    try {
      // eslint-disable-next-line camelcase
      const data = await get<{ auth_token: string, user: UserModel }>({
        url: config.apiUrl,
        urlPath: 'auth/info',
        data: {
          auth_token: authStore.token
        }
      });
      transaction(() => {
        authStore.setUser(data.data.user);
        authStore.setToken(data.data.auth_token);
      });
    } catch (err) {
      this.logOut();
      authStore.setErrorMessage(err.response?.data?.data.message || 'Network error');
    }
  }

  async logIn(): Promise<void> {
    const { login, password } = authStore.loginInfo;
    if (!this.validateLoginField()) {
      return;
    }
    authStore.setStatus(LOGIN_STATUS.PENDING);
    try {
      // eslint-disable-next-line camelcase
      const data = await post<{ auth_token: string, user: UserModel }>({
        url: config.apiUrl,
        urlPath: 'auth/login',
        data: { login: login.value.replace(/\s/g, ''), password: password.value }
      });
      transaction(() => {
        authStore.setToken(data.data.auth_token);
        authStore.setUser(data.data.user);
        authStore.setLoginDialogOpened(false);
        authStore.setLoginInfo({
          login: {
            value: '',
            isInvalid: false,
            isValid: false,
          },
          password: {
            value: '',
            isInvalid: false,
            isValid: false,
          },
        });
      });
    } catch (err) {
      authStore.setStatus(LOGIN_STATUS.NOTLOGGEDIN);
      authStore.setErrorMessage(err.response?.data?.data.message || 'Network error');
    }
  }

  // eslint-disable-next-line class-methods-use-this
  async logOut(): Promise<void> {
    const urlPath = 'auth/logout';

    try {
      await post({
        url: config.apiUrl,
        urlPath,
        data: { auth_token: authStore.token }
      });
    } finally {
      transaction(() => {
        authStore.setUser(null);
        authStore.setToken(null);
      });
    }
  }

  // eslint-disable-next-line class-methods-use-this
  updateLoginField(field: keyof LoginInfoModel, value: string): void {
    authStore.setLoginInfo({
      ...authStore.loginInfo,
      [field]: {
        value,
        isInvalid: !value,
        isValid: !!value,
      }
    });
  }

  // eslint-disable-next-line class-methods-use-this
  validateLoginField(): boolean {
    const { login, password } = authStore.loginInfo;
    authStore.setLoginInfo({
      login: {
        value: login.value,
        isInvalid: !login.value,
        isValid: !!login.value,
      },
      password: {
        value: password.value,
        isInvalid: !password.value,
        isValid: !!password.value,
      }
    });
    return !!login.value && !!password.value;
  }

}
