import config from 'Root/config';
import { get } from 'Root/utils/api';
import { ItemsCount } from 'Specs/models';
import authStore from 'Stores/authStore';
import summaryInfoStore from 'Stores/summaryInfoStore';

export class SummaryInfoService {

  // eslint-disable-next-line class-methods-use-this
  async getSummaryInfo(): Promise<void> {
    try {
      const data = await get<{ catalog: ItemsCount, videos: ItemsCount }>({
        url: config.apiUrl,
        urlPath: 'summary/list',
        data: {
          auth_token: authStore.token
        }
      });
      summaryInfoStore.setItemsCountInfo(data.data.catalog, data.data.videos);
    } catch (err) {
      summaryInfoStore.setErrorMessage(err.response?.data?.data.message || 'Network error');
    }
  }

}
