## Video catalog front 
Videos catalog site & app

## Installation

Use a package manager npm in order to install all dependencies

```bash
npm install
```

## Usage
To run electron app:

In order to run this project 2 scripts will need to be executed `dev:react` and `dev:electron`, run each one in a different terminal and always run `dev:react` before `dev:electron`, or `dev` to run them in order automatically

```bash
npm run dev:react
```
```bash
npm run dev:electron
```

or

```bash
npm run dev
```

To run as site:

```bash
npm run dev:site
```

Check code style:

```bash
npm run lint
```

Run tests:

```bash
npm run test
```

## Packaging
Build elecron app:

To generate a project package run `package`

```bash
npm run package
```

Generate production site build
```bash
npm run build:site
```
Run production site build
```bash
npm run production:site
```

## Dependencies

- react
- mobx
- styled-components
- @mui/material
- axios
- express
- typescript
- webpack

## Repository

https://gitlab.com/umcv_catalog/front

## License

[MIT](https://choosealicense.com/licenses/mit/)
