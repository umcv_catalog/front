module.exports = {

  // Automatically clear mock calls and instances between every test
  clearMocks: true,

  // Indicates whether the coverage information should be collected while executing the test
  collectCoverage: true,

  // The maximum amount of workers used to run your tests. Can be specified as % or a number.
  // E.g. maxWorkers: 10% will use 10% of your CPU amount + 1 as the maximum worker number.
  maxWorkers: 10,

  // An array of file extensions your modules use
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],

  // A map from regular expressions to module names or to arrays of module names that allow to stub out resources with a single module
  moduleNameMapper: {
    '^.+\\.(css|less|scss|po|png|jpg|svg)$': 'ts-jest',
    '^Components(.*)$': '<rootDir>/src/components$1',
    '^Hoc(.*)$': '<rootDir>/src/hoc$1',
    '^Pages(.*)$': '<rootDir>/src/pages$1',
    '^Root(.*)$': '<rootDir>/src$1',
    '^Router$': '<rootDir>/src/Router',
    '^Services(.*)$': '<rootDir>/src/services$1',
    '^Specs(.*)$': '<rootDir>/src/specs$1',
    '^Stores(.*)$': '<rootDir>/src/stores$1',
    '^Utils(.*)$': '<rootDir>/src/utils$1'
  },

  snapshotSerializers: [
    'enzyme-to-json/serializer'
  ],

  setupFiles: [
    './src/utils/setupTests.ts'
  ],

  setupFilesAfterEnv: [
    './src/utils/setupTestHooks.ts'
  ],

  // A preset that is used as a base for Jest's configuration
  preset: 'ts-jest',

  // The test environment that will be used for testing
  testEnvironment: 'jsdom',

  // A map from regular expressions to paths to transformers
  transform: {
    '^.+\\.(ts|tsx)?$': 'ts-jest',
  },
};
