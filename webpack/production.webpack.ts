import LoadablePlugin from '@loadable/webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import path from 'path';
import webpack, { Configuration } from 'webpack';
import merge from 'webpack-merge';
import WebpackErrorNotificationPlugin from 'webpack-notifier';
import WebpackBar from 'webpackbar';

import resolve from './resolve.webpack';
import rules from './rules.webpack';

const rootPath = path.resolve(__dirname, '..');

const config: Configuration = {
  entry: path.resolve(rootPath, 'src', 'index.tsx'),
  mode: 'production',
  output: {
    filename: 'js/[name].js',
    path: path.resolve(rootPath, 'dist/renderer'),
    publicPath: './'
  },
  plugins: [
    new WebpackBar({}),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([
      {
        from: path.resolve(rootPath, 'sources'),
        to: path.resolve(rootPath, 'dist/renderer')
      },
    ]),
    new webpack.DefinePlugin({
      'process.env': {
        BROWSER: JSON.stringify(true),
        DEBUG: JSON.stringify(false),
        NODE_ENV: JSON.stringify('production'),
      }
    }),
    new WebpackErrorNotificationPlugin(),
    new LoadablePlugin({ filename: 'stats.json', writeToDisk: { filename: path.resolve(rootPath, 'dist/renderer') } }),
  ],
};

export default merge(resolve, rules, config);
