import path from 'path';
import { Configuration } from 'webpack';
import merge from 'webpack-merge';

import resolve from './resolve.webpack';
import rules from './rules.webpack';

const rootPath = path.resolve(__dirname, '..');

const config: Configuration = {
  devtool: 'source-map',
  entry: path.resolve(rootPath, 'electron', 'main.ts'),
  node: {
    __dirname: false
  },
  output: {
    filename: '[name].js',
    path: path.resolve(rootPath, 'dist'),
  },
  target: 'electron-main',
};

export default merge(resolve, rules, config);
