import HtmlWebpackPlugin from 'html-webpack-plugin';
import path from 'path';
import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';

const rootPath = path.resolve(__dirname, '..');

module.exports = {
  devServer: {
    compress: true,
    contentBase: path.join(rootPath, 'dist/renderer'),
    historyApiFallback: true,
    host: '0.0.0.0',
    hot: true,
    port: 4000,
    publicPath: '/'
  },
  devtool: 'source-map',
  entry: path.resolve(rootPath, 'src', 'index.tsx'),
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        exclude: /node_modules/,
        test: /\.(ts|tsx)$/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
  output: {
    filename: 'js/[name].js',
    path: path.resolve(rootPath, 'dist/renderer'),
    publicPath: './'
  },
  plugins: [
    new HtmlWebpackPlugin()
  ],
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    mainFields: ['main', 'module', 'browser'],
    plugins: [new TsconfigPathsPlugin()],
  },
  target: 'electron-renderer',
};
