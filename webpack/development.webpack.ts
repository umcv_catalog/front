import LoadablePlugin from '@loadable/webpack-plugin';
import { CleanWebpackPlugin } from 'clean-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import path from 'path';
import webpack, { Configuration } from 'webpack';
import merge from 'webpack-merge';
import WebpackErrorNotificationPlugin from 'webpack-notifier';
import WebpackBar from 'webpackbar';

import resolve from './resolve.webpack';
import rules from './rules.webpack';

const host = '127.0.0.1';
const port = (parseInt(process.env.PORT || '', 10) || 3000);
const rootPath = path.resolve(__dirname, '..');

const config: Configuration = {
  devtool: 'source-map',
  entry: path.resolve(rootPath, 'src', 'index.tsx'),
  mode: 'development',
  output: {
    filename: 'js/[name].js',
    path: path.resolve(rootPath, 'dist/renderer'),
    publicPath: `http://${host}:${port + 1}/`
  },
  plugins: [
    new WebpackBar({}),
    new CleanWebpackPlugin(),
    new CopyWebpackPlugin([
      {
        from: path.resolve(rootPath, 'sources'),
        to: path.resolve(rootPath, 'dist/renderer')
      },
    ]),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        BROWSER: JSON.stringify(true),
        DEBUG: JSON.stringify(false),
        NODE_ENV: JSON.stringify('development'),
      }
    }),
    new WebpackErrorNotificationPlugin(),
    new LoadablePlugin({ filename: 'stats.json', writeToDisk: { filename: path.resolve(rootPath, 'dist/renderer') } }),
  ],
  watchOptions: {
    ignored: ['../dist/**']
  },
};

export default merge(resolve, rules, config);
