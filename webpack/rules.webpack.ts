import { Configuration } from 'webpack';

const rules: Partial<Configuration> = {
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        exclude: /node_modules/,
        test: /\.(ts|tsx)$/,
        use: ['babel-loader', 'ts-loader']
      }
    ]
  },
};

export default rules;
