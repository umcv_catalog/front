import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin';
import { Configuration } from 'webpack';

const resolve: Partial<Configuration> = {
  resolve: {
    extensions: ['.tsx', '.ts', '.js'],
    mainFields: ['main', 'module', 'browser'],
    modules: ['node_modules'],
    plugins: [new TsconfigPathsPlugin()],
  },
};

export default resolve;
