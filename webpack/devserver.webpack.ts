import { Server } from 'http';
import path from 'path';
import webpack, { Configuration } from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import merge from 'webpack-merge';

import resolve from './resolve.webpack';
import rules from './rules.webpack';

const rootPath = path.resolve(__dirname, '..');
const host = '127.0.0.1';
const port = (parseInt(process.env.PORT || '', 10) || 3000);

const options: WebpackDevServer.Configuration = {
  compress: false,
  contentBase: [`http://${host}:${port + 1}`, path.resolve(rootPath, 'dist/renderer/')],
  headers: {
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept, Cache-Control',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
    'Access-Control-Allow-Origin': '*'
  },
  historyApiFallback: {
    disableDotRule: true
  },
  hot: true,
  inline: false,
  lazy: false,
  overlay: true,
  port: port + 1,
  publicPath: `http://${host}:${port + 1}/`,
  stats: {
    colors: true
  },
};

const config: Partial<Configuration> = {
  devtool: 'source-map',
  entry: path.resolve(rootPath, 'src', 'index.tsx'),
  mode: 'development',
  output: {
    filename: 'js/[name].js',
    path: path.resolve(rootPath, 'dist/renderer'),
    publicPath: `http://${host}:${port + 1}/`
  },
};

export default function devServer(): Server {
  const compiler = webpack(merge(resolve, rules, config));
  const webpackDevServer = new WebpackDevServer(compiler, options);

  return webpackDevServer.listen(port + 1, host);
}
